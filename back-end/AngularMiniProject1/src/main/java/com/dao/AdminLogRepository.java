package com.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.bean.AdminLogin;

public interface AdminLogRepository extends CrudRepository<AdminLogin, Integer>{
	List<AdminLogin> findAll();
}



